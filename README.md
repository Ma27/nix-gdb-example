#  nix-gdb-example

Just a simple test environment to play around with debugging-symbols for `gdb` when building stuff with Nix.

`main.c` compiles to a simple program that segfaults on purpose to test gdb's capabilities.

## Usage

To locally compile it with debug symbols:

```
gcc -O0 -g main.c
gdb ./a.out
```

To read the file from a store-path (without debug-symbols):

```
nix-build
gdb ./result/bin/main
```

To play around with debugging symbols on NixOS:

``` nix
{
  environment.enableDebugInfo = true;
  environment.systemPackages = [
    (import /path/to/nix-gdb-example)
  ];
}
```

And then:

```
$ gdb /run/current-system/sw/bin/main
```

Or when using home-manager (with a [patch for debugging symbols](https://github.com/rycee/home-manager/pull/1040)):

``` nix
{
  home.packages = [ (import /path/to/nix-gdb-example) ];
  home.enableDebugInfo = true;
}
```
