with import <nixpkgs> { };

stdenv.mkDerivation {
  name = "snens";
  src = ./.;
  buildPhase = ''
    gcc -O0 -ggdb -gsplit-dwarf main.c
  '';
  installPhase = ''
    mkdir -p $out/bin
    cp a.out $out/bin/main
  '';
  separateDebugInfo = true;
  outputs = [ "out" ];
}
